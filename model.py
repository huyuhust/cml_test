import torch
import torch.nn as nn
import torch.nn.functional as F

class CNN(nn.Module):
    def __init__(self):
        super(CNN, self).__init__()
        self.conv1 = nn.Sequential( # (1,8,8)
                     nn.Conv2d(in_channels=1, out_channels=16, kernel_size=3, stride=1, padding=1), # (132,8,8)
                     nn.ReLU(),
                     nn.MaxPool2d(kernel_size=2,stride=1) # (32,7,7)
                     )
        self.conv2 = nn.Sequential( # (32,7,7)
                     nn.Conv2d(16, 32, 3, 1, 1), # (64,7,7)
                     nn.ReLU(),
                     nn.MaxPool2d(2,1) # (64,6,6)
		     )
        self.fc1 = nn.Linear(32*6*6,32)
        self.fc2 = nn.Linear(32,16)
        self.out = nn.Linear(16,2)

 
    def forward(self, x):
        x = self.conv1(x)
        x = self.conv2(x)
        x = x.view(x.size(0), -1) # flat batch 32,7,7 to (batch_size, 32*7*7)
        x = F.relu(self.fc1(x))
        x = F.relu(self.fc2(x))
        output = self.out(x)
        return output
 





