import torch
import torch.nn as nn
from torch.autograd import Variable
from  torch.optim import lr_scheduler
import torch.utils.data as Data
import torch.nn.functional as F
import torchvision
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
#import tables
from torchvision import transforms
#from logger import Logger
from model import CNN
import h5py

 
torch.manual_seed(1)
 
EPOCH = 50
BATCH_SIZE = 256
BATCH_SIZE_test = 100
LR = 0.001

def train(pklfile):
        np.random.seed(12237)		
        # input dataset from h5, then divide it into train dataset and test dataset(4:1)
        h5 = h5py.File('/home/huy/workarea/positron_location/data_nor.h5', 'r')
        data = h5['data1']['data'][:]
        lable = h5['data1']['lable'][:]
        np.random.seed(12237)		
        np.random.shuffle(data)	
        np.random.seed(12237)		
        np.random.shuffle(lable)	

        train_np = data[ : int(data.shape[0]*9/10)]
        train_labels_np = lable[ : int(lable.shape[0]*9/10)] 
        test_np  = data[int(data.shape[0]*9/10) : ]
        test_labels_np = lable[int(lable.shape[0]*9/10) : ]
        
        # train dataset
        train_data_tensor = torch.from_numpy(train_np.reshape((train_np.shape[0],1,8,8))).double()
        train_labels_tensor = torch.from_numpy(train_labels_np).double()
        train_dataset   = Data.TensorDataset(train_data_tensor, train_labels_tensor)
        train_loader = Data.DataLoader(train_dataset, batch_size=BATCH_SIZE, shuffle=True)
        
        #test dataset	
        test_data_tensor = torch.from_numpy(test_np.reshape((test_np.shape[0],1,8,8))).double()
        test_labels_tensor = torch.from_numpy(test_labels_np).double()
        test_dataset   = Data.TensorDataset(test_data_tensor, test_labels_tensor)
        test_loader   = Data.DataLoader(test_dataset, batch_size=BATCH_SIZE_test)
        
         
        cnn = CNN()
        cnn = cnn.cuda()
        cnn = cnn.double()
        #print(cnn)
        
        optimizer = torch.optim.SGD(cnn.parameters(), lr=LR, weight_decay=1e-1)
        scheduler = lr_scheduler.StepLR(optimizer,step_size=5,gamma = 0.7) 		
        loss_func = nn.MSELoss()
         
        plt.ion()
        step_list = []
        loss_list = []
        loss_list_test = []
        
        for epoch in range(EPOCH):
            running_loss = 0.0
            n_bach = 0
            for step, data in enumerate(train_loader):
                b_X, b_Y = data
                b_x = b_X.cuda()
                b_y = b_Y.cuda()
         
                output = cnn(b_x).cuda()
                loss = loss_func(output, b_y)
                optimizer.zero_grad()
                loss.backward()
                optimizer.step()
                running_loss += loss.item()
                n_bach +=1
         
            scheduler.step()
            loss_train = running_loss/n_bach
            running_loss = 0.0
            n_bach = 0

            test_output = cnn(test_data_tensor[:20000].cuda())
            pred_y = test_output.cpu().data.numpy()
            accuracy = sum(sum(pred_y - test_labels_np[:20000])/20000.)/2.
            loss_test = loss_func(test_output, test_labels_tensor[:20000].cuda())
            print(accuracy)
            print('Epoch:', epoch, '|train loss:%.4f'%loss_train, '|test loss:%.4f'%loss_test.item(), '|test accuracy:%.4f'%accuracy)
            step_list.append(epoch)
            loss_list.append(loss_train)
            loss_list_test.append(loss_test.item())	
            
            plt.cla()
            plt.plot(step_list, loss_list, 'b-', lw=1, label='train')	
            plt.plot(step_list, loss_list_test, 'r-', lw=3, label='test')	
            plt.xlabel('step')	
            plt.ylabel('loss')	
            plt.legend(loc = 'best')	
            plt.pause(0.1)

        plt.ioff()
        #plt.show()
        plt.savefig('train_loss.png')

        # The model after train        
        #for name, param in cnn.state_dict().items():
        #print(name, param.size())

        print('**save the whole model') 	     
        #torch.save(model_object, 'model.pkl') # save the whole model
        torch.save(cnn.state_dict(), pklfile) # only save the parameters ((recommended))

def test(pklfile):
        cnn = CNN()
    #    cnn = cnn.cuda()
        cnn.load_state_dict(torch.load(pklfile))		
        cnn = cnn.double()		
        cnn.eval()

        h5 = h5py.File('/home/huy/workarea/positron_location/data_nor.h5', 'r')
        data = h5['data1']['data'][:]
        lable = h5['data1']['lable'][:]
        np.random.seed(12237)		
        np.random.shuffle(data)	
        np.random.seed(12237)		
        np.random.shuffle(lable)	

        test_np  = data[int(data.shape[0]*9/10) : ]
        test_labels_np = lable[int(lable.shape[0]*9/10) : ]

        #test dataset	
        test_data_tensor = torch.from_numpy(test_np.reshape((test_np.shape[0],1,8,8))).double()
        test_labels_tensor = torch.from_numpy(test_labels_np).double()
        test_output = cnn(test_data_tensor[20000:])
        pred_y = test_output.cpu().data.numpy()
        print(pred_y[0:5], test_labels_np[20000:20005])		
        with h5py.File('test_result_nor.h5', 'w') as hf1:
                g2 = hf1.create_group('test')
                g2.create_dataset("predict",  data = pred_y)
                g2.create_dataset("lable",  data = test_labels_np[20000:])
        
if __name__ == '__main__':			 
		pklfile = 'pkl/model_nor.pkl'
		train(pklfile)
		#test(pklfile)

